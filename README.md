#HTML

These Html templates were designed for the Diaspora social networking 
platform. There is a templete for each show in the following in order:  

* Show Title (with link to show page)
* Epsode Number
* Episode Title
* Episode Thumbnail Image (with link to episode page)
* Episode Description
* BtSync Secret
* Bittorrent Links
* RSS Feed Links
* Direct Download Links
* Contact Links
* Official Website Link and Donation Link.
* Affiliate Links
* Hash Tags
* Youtube Video Link


Examples of finished posts can be seen [Here](https://joindiaspora.com/tags/jupiterbroadcasting)  

Most images are hosted via imgur with the exception of episode thumbnails.


#Shell Scripts  

This repository also includes shell scripts which I use to more easily manage BtSync shares for each show. For the moment these are very primative while I come up with better methods of automation. These scripts call BASH (Bourne Again Shell) but will probably work just fine with SH. The structure of these scripts consists of the following in order:

* Variable containing video Link
* Variable containing MP3 Link
* Variable containing OGG Link
* Variable containing Destination directory
* Function to check if destination directory exists
* Remove MP4, MP3, OGG
* Wget Video var, MP3 var, OGG var
* Execute directory check function at runtime

---
#Disclaimer


This project was created and is maintained by a volunteer and is not officially affiliated with Jupiter Broadcasting in any way.  
All original content is provided as gratis to further the success of Jupiter Broadcasting.  
I am not responsible for any person who chooses to use these templates.
